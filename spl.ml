(* Simple logic language *)

(* Input *)

type pos = {
    col: int;
    line: int;
}

let show_pos { col; line } = string_of_int col ^ ":" ^ string_of_int line

let advance_pos { col; line } c =
    if c = '\n' then { col = 0;       line = line + 1 }
                else { col = col + 1; line = line     }

(* type source = Str of string | Ic of In_channel.t | Stdin *)

type input = {
    idx: int;
    str: string;
    pos: pos;
}

let input_string str = { idx = 0; str = str; pos = { col = 0; line = 1 } }
let input_ic ic      = input_string (In_channel.input_all ic)
let input_file fname = In_channel.with_open_text fname input_ic

let peek_char input =
    if input.idx < String.length input.str
    then Some (input.str.[input.idx]) else None

let advance input =
    let input = { input with idx = input.idx + 1 } in
    match peek_char input with
    | None -> input
    | Some c -> { input with pos = advance_pos input.pos c }


(* Lexer *)

type tok = LPAR | RPAR | ID of string | EOF

let rec next_tok input =

    let rec lex_until acc pred input =
        match peek_char input with
        | None -> acc, input
        | Some c when pred c -> acc, input
        | Some c -> lex_until (acc ^ (String.make 1 c)) pred (advance input)
    in

    let is_space = function ' ' | '\n' | '\r' | '\t' -> true | _ -> false in
    let is_sep   = function ';' | '(' | ')' -> true | c -> is_space c in

    match peek_char input with
    | None -> EOF, input
    | Some c ->

    let input = advance input in

    match c with
    | '(' -> LPAR, input
    | ')' -> RPAR, input
    | ';' -> let _, input = lex_until "" ((=) '\n') input in next_tok input
    | c when is_space c -> next_tok input
    | c -> let str, input = lex_until (String.make 1 c) is_sep input in ID str, input


(* Parser *)

type term = Var  of string
          | Atom of string
          | List of term list

let rec show_term = function
    | Var v -> v
    | Atom a -> a
    | List terms -> "(" ^ (terms |> List.map show_term |> String.concat " ") ^ ")"


let rec parse_term tok input =
    let is_var s = match s.[0] with '_' | 'A'..'Z' -> true |  _ -> false in

    match tok with
    | EOF  -> Error "Unexpected EOF"
    | RPAR -> Error "Unexpected RPAR"
    | ID s when is_var s -> Ok (Var s, input)
    | ID s -> Ok (Atom s, input)
    | LPAR ->

    match parse_list RPAR [] input with
    | Error _ as e -> e
    | Ok (terms, input) -> Ok (List terms, input)

and parse_list upto acc input =
    let tok, input = next_tok input in
    if tok = upto then Ok (List.rev acc, input) else
    match parse_term tok input with
    | Error _ as e -> e
    | Ok (term, input) -> parse_list upto (term :: acc) input

let parse_next input =
    let tok, input = next_tok input in
    match tok with
    | EOF -> None
    | tok -> Some (parse_term tok input)


(* Solver *)
(*
    https://www.cs.cornell.edu/courses/cs3110/2011sp/Lectures/lec26-type-inference/type-inference.htm#unify
    https://github.com/Naereen/Tiny-Prolog-in-OCaml/blob/master/prolog/resolution.ml
    https://epilys.github.io/ocaml-prolog/
*)

(* Sort binding on variable name *)
let sort_bindings bindings =
    List.sort (fun (v, _) (v', _) -> String.compare v v') bindings

let show_bindings_pretty sep bindings =
    bindings |> sort_bindings
             |> List.map (fun (v, e) -> v ^ " = " ^ show_term e)
             |> String.concat sep

(* True if var is found in term *)
let rec var_occurs var term =
    match term with
    | List terms -> List.exists (var_occurs var) terms
    | t -> t = Var var

(* Replace pat with sub in term *)
let rec subst pat sub term =
    match term with
    | List terms -> List (List.map (subst pat sub) terms)
    | t when t = pat -> sub
    | t -> t

(* Replace binded variables with their value *)
let expand bindings term =
    List.fold_right (fun (v, t) acc -> subst (Var v) t acc) bindings term
    (* fold_right because older bindings are at the end ?
    List.fold_left (fun acc (v, t) -> subst (Var v) t acc) term bindings
    *)

(* Expand variables in binded values *)
let normalize bindings =
    List.map (fun (v, t) -> v, expand bindings t) bindings

(* Return Some bindings if a unifies with b *)
let rec unify bindings a b =
    match a, b with
    | Var "_", _ | _, Var "_" -> Some bindings
    | Var v, t | t, Var v ->
        if a = b then Some bindings else
        if var_occurs v t then None else
        Some (normalize ((v, t) :: bindings))
    | List xs, List ys ->
        if List.length xs <> List.length ys then None else
        unify_lists bindings xs ys
    | _, _ when a = b -> Some bindings
    | _ -> None

(* Unify lists of terms *)
and unify_lists bindings xs ys =
    match xs, ys with
    | [], [] -> Some bindings
    | [], _ | _, [] -> None
    | x::xs, y::ys ->

    let x = expand bindings x
    and y = expand bindings y in

    match unify bindings x y with
    | None -> None
    | Some bindings ->

    unify_lists bindings xs ys

(* Generate new variables *)
let symcount = ref 0
let gensym () =
    let v = "_X" ^ string_of_int !symcount in
    incr symcount;
    Var v

(* Rename variables in goal (term) when they're found in query (term) *)
let rec alpha_renaming query goal =
    match goal with
    | List terms -> List (List.map (alpha_renaming query) terms)
    | Var v when var_occurs v query -> gensym ()
    | _ -> goal


(* Find clauses where the head unifies with goal *)
let match_clauses facts query goal =
    List.filter_map (fun (head, body) ->
        let head' = alpha_renaming query head in
        match unify [] goal head' with
        | None -> None
        | Some bindings -> Some (bindings, head, body)
    ) (List.rev facts)


let rec prove_goals facts query goals choices =
    match goals with
    | [] -> Some (query, choices) (* No more goals to prove: OK *)
    | goal :: goals ->

    match goal with
    | List [Atom "write"; t] -> print_string (show_term t); prove_goals facts query goals choices
    | Atom "nl" -> print_newline (); prove_goals facts query goals choices
    | Atom "halt" -> exit 0
    | Atom "fail" -> next_choice facts choices (* FAIL *)
    | Atom "!" -> prove_goals facts query goals [] (* Continue with no other choice *)
    | _ ->

    let clauses = match_clauses facts query goal in
    prove_subgoal facts query goals clauses choices


and prove_subgoal facts query goals clauses choices =
    match clauses with
    | [] -> next_choice facts choices (* No matching clauses: FAIL *)
    | (bindings, head, body) :: clauses ->

    (* Record backtrack point *)
    let choices = (query, goals, clauses) :: choices in

    (* Add clause body to goals and apply bindings *)
    let goals = List.map (expand bindings) (body @ goals)
    and query = expand bindings query in

    (* Continue with next goal *)
    prove_goals facts query goals choices

and next_choice facts choices =
    match choices with
    | [] -> None
    | (query, goals, clauses) :: choices ->
        prove_subgoal facts query goals clauses choices


(* Eval *)

let interactive_solve facts goals =
    let query = List goals in

    let rec success_loop sol choices =
        match unify [] query sol with
        | None -> failwith "never fails"
        | Some bindings ->

        if bindings = [] then print_string "yes. "
        else print_string ("yes. " ^ show_bindings_pretty ", " bindings ^ " ");

        Out_channel.flush Out_channel.stdout;
        match In_channel.input_line In_channel.stdin with
        | None -> print_newline () (* EOF Abort *)
        | Some "a" -> () (* Abort *)
        | _ ->

        match next_choice facts choices with
        | None -> () (* No more solutions *)
        | Some (sol, choices) -> success_loop sol choices (* Continue *)
    in

    match prove_goals facts query goals [] with
    | None -> print_endline "no."
    | Some (sol, choices) -> success_loop sol choices


let rec eval facts term =
    match term with
    | Var v -> Error ("Cannot eval var " ^ v)
    | List (head :: Atom ":-" :: body) -> Ok ((head, body) :: facts)
    | List (Atom "?-" :: goals) -> let _ = interactive_solve facts goals in Ok facts
    (* | List (Atom ":-" :: goals) -> let _ = prove_goals facts (List goals) goals [] in Ok facts *)
    | head -> Ok ((head, []) :: facts)


let rep facts input =
    match parse_next input with
    | None -> None
    | Some (Error e) -> Some (Error ("Parser error: " ^ show_pos input.pos ^ ":" ^ e))
    | Some (Ok (term, input)) ->

    match eval facts term with
    | Error e -> Some (Error ("Error: " ^ show_pos input.pos ^ " " ^ e))
    | Ok facts -> Some (Ok (facts, input))


let rec rep_all facts input =
    match rep facts input with
    | None -> Ok facts
    | Some (Error e) -> Error e
    | Some (Ok (facts, input)) -> rep_all facts input


let rec repl facts =
    print_string "spl> "; flush stdout;

    match In_channel.input_line In_channel.stdin with
    | None -> print_endline "Bye!"
    | Some line ->

    match rep_all facts (input_string line) with
    | Error e  -> print_endline e
    | Ok facts -> repl facts


let rec run facts interactive = function
    | [] -> if interactive then repl facts
    | "-i" :: args -> run facts true args
    | file :: args ->

    match rep_all facts (input_file file) with
    | Error e -> print_endline e
    | Ok facts -> run facts interactive args

let () =
    match Array.to_list Sys.argv with
    | [] | [_] -> repl []
    | _::args -> run [] false args
