#mod_use "spl.ml"
open Spl

let pass = None
let fail msg = Some msg
let expected a b = fail ("Expected " ^ a ^ " but got " ^ b)


let test_unification =
    let show = function
        | None -> "None"
        | Some [] -> "(empty)"
        | Some bindings -> show_bindings_pretty ", " bindings
    in

    let expect exp a b () =
        let res = unify [] a b in

        let res = Option.map sort_bindings res in
        let exp = Option.map sort_bindings exp in

        if exp = res then pass else
        expected (show exp) (show res)
    in

    "Unification", [
        "X _",    expect (Some [])              (Var "X")  (Var "_");
        "X a",    expect (Some ["X", Atom "a"]) (Var "X")  (Atom "a");
        "a X",    expect (Some ["X", Atom "a"]) (Atom "a") (Var "X");
        "X X",    expect (Some [])              (Var "X")  (Var "X");
        "X Y",    expect (Some ["X", Var "Y"])  (Var "X")  (Var "Y");
        "foo(X) foo(a)", expect (Some ["X", Atom "a"]) (List [Atom "foo"; Var "X"])  (List [Atom "foo"; Atom "a"]);
        "foo(a) foo(X)", expect (Some ["X", Atom "a"]) (List [Atom "foo"; Atom "a"]) (List [Atom "foo"; Var "X"]);

        "foo(X,Y) foo(Y,X)",  expect (Some ["X", Var "Y"])               (List [Atom "foo"; Var "X"; Var "Y"]) (List [Atom "foo"; Var "Y"; Var "X"]);
        "foo(X,X) foo(Y,Z)",  expect (Some ["X", Var "Z"; "Y", Var "Z"]) (List [Atom "foo"; Var "X"; Var "X"]) (List [Atom "foo"; Var "Y"; Var "Z"]);
        "foo(X,Y) foo(Z,X)",  expect (Some ["X", Var "Z"; "Y", Var "Z"]) (List [Atom "foo"; Var "X"; Var "Y"]) (List [Atom "foo"; Var "Z"; Var "X"]);

        "foo(X,Y,Z,W) foo(W,X,Y,Z)",    expect (Some ["X", Var "W"; "Y", Var "W"; "Z", Var "W"])                    (List [Atom "foo"; Var "X"; Var "Y"; Var "Z"; Var "W"]) (List [Atom "foo"; Var "W"; Var "X"; Var "Y"; Var "Z"]);
        "foo(X,Y,Z,W) foo(Y,Z,W,_)",    expect (Some ["X", Var "W"; "Y", Var "W"; "Z", Var "W"])                    (List [Atom "foo"; Var "X"; Var "Y"; Var "Z"; Var "W"]) (List [Atom "foo"; Var "Y"; Var "Z"; Var "W"; (Var "_")]);
        "foo(X,Y,Z,W) foo(Y,Z,W,a)",    expect (Some ["W", Atom "a"; "X", Atom "a"; "Y", Atom "a"; "Z", Atom "a"])  (List [Atom "foo"; Var "X"; Var "Y"; Var "Z"; Var "W"]) (List [Atom "foo"; Var "Y"; Var "Z"; Var "W"; Atom "a"]);

        "foo(X,foo(Y, Z),W) foo(A, B, C)", expect (Some ["B", List [Atom "foo"; Var "Y"; Var "Z"]; "X", Var "A"; "W", Var "C"])  (List [Atom "foo"; Var "X"; List [Atom "foo"; Var "Y"; Var "Z"]; Var "W"]) (List [Atom "foo"; Var "A"; Var "B"; Var "C"]);

        "a b",                    expect None (Atom "a") (Atom "b");
        "X foo(X)",               expect None (Var "X") (List [Atom "foo"; Var "X"]);
        "foo(X, X) foo(a, b)",    expect None (List [Atom "foo"; Var "X"; Var "X"]) (List [Atom "foo"; Atom "a"; Atom "b"]);
        "foo(a) bar(a)",          expect None (List [Atom "foo"; Atom "a"]) (List [Atom "bar"; Atom "a"]);
    ]


let test_parser =
    let show = function
        | Error e -> "Error: " ^ e
        | Ok t -> show_term_pretty (List t)
    in

    let expect exp str () =
        let res = parse_string str in
        if exp = res then pass else
        expected (show exp) (show res)
    in

    "Parser", [
        "a",    expect (Ok [Atom "a"]) "a";
        "a bc", expect (Ok [Atom "a"; Atom "bc"]) "a bc";
        "a A _b _B Bx cB", expect (Ok [Atom "a"; Var "A"; Var "_b"; Var "_B"; Var "Bx"; Atom "cB"]) "a A _b _B Bx cB";
        "  (a (b c)) d\\n    e (f  (\\n  ghi \\n) ())  () (  )  J  ", expect (Ok [List [Atom "a"; List [Atom "b"; Atom "c";]]; Atom "d"; Atom "e"; List [Atom "f"; List [Atom "ghi"]; List []]; List []; List []; Var "J"]) "  (a (b c)) d\n    e (f  (\n  ghi \n) ())  () (  )  J  ";
    ]


let run_test (name, cases) (passed, failed) =
    let red   = "\x1b[31m"
    and green = "\x1b[32m"
    and reset = "\x1b[m" in

    Printf.printf "Testing %s:\n" name;
    List.fold_left (fun (p, f) (name, fn) ->
        match fn () with
        | None ->
            let _ = Printf.printf "  %sPASS:%s \"%s\"\n" green reset name in
            p+1, f
        | Some msg ->
            let _ = Printf.printf "  %sFAIL:%s \"%s\" %s\n" red reset name msg in
            p, f+1
    ) (passed, failed) cases

let () =
    let passed, failed =
        (0, 0)
        |> run_test test_unification
        |> run_test test_parser
    in
    Printf.printf "Passed: %d  Failed: %d\n" passed failed
